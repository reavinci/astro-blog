<?php
namespace Astro_Blog\Elementor;

class Elementor_Init
{

    public function __construct()
    {

        add_action('elementor/widgets/widgets_registered', array($this, 'on_widgets_registered'));
        add_action('elementor/init', array($this, 'register_category'));

    }

    public function on_widgets_registered()
    {
        $this->include_part();
        $this->register_widget();
    }

    public function include_part()
    {

        include dirname(__FILE__) . '/post/posts.php';
        include dirname(__FILE__) . '/post/posts-list.php';

        if (astro_is_premium()) {
            include dirname(__FILE__) . '/post/posts-block.php';
            include dirname(__FILE__) . '/post/posts-block-2.php';
            include dirname(__FILE__) . '/post/posts-block-3.php';
            include dirname(__FILE__) . '/post/posts-block-4.php';

            include dirname(__FILE__) . '/post/posts-tiles.php';
            include dirname(__FILE__) . '/post/posts-slider.php';
            include dirname(__FILE__) . '/post/posts-slider-thumb.php';
            include dirname(__FILE__) . '/post/posts-smart-tiles.php';
        }

    }

    /**
     * register Category
     * @since 1.0.0
     * @return [add new category]
     */
    public function register_category()
    {

        \Elementor\Plugin::instance()->elements_manager->add_category(
            'astro-blog',
            [
                'title' => 'Astro Blog',
                'icon' => 'font',
            ],
            1
        );

    }

    /**
     * Register Widget
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function register_widget()
    {
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_List());
        if (astro_is_premium()) {
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Block());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Block_2());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Block_3());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Block_4());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Tiles());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Slider());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Slider_Thumb());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts_Smart_Tiles());
        }

    }

    // end class
}

new Elementor_Init;
