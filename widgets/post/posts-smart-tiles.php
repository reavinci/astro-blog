<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Astro_Blog\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_Smart_Tiles extends Posts
{
    public function get_name()
    {
        return 'astro-posts-smart-tiles';
    }

    public function get_title()
    {
        return __('Posts Smart Tiles', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-smart-tiles';
    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query(array(
            'limit_perpage' => true,
        ));
        $this->setting_options(); //protected

        $this->style_title_tiles();
        $this->style_badges();
        $this->style_meta();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();
    }



    public function style_title_tiles()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__title a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__title a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => __('Primary Typography', 'astro-blog'),
                'selector' => '{{WRAPPER}} .at-smart-tiles__main .at-post__title',
                'separator' => 'after',
            ]
        );

        $this->add_responsive_control(
            'title_main_spacing',
            [
                'label' => __('Title Primary Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-smart-tiles__main .at-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_group_typography',
                'label' => __('Group Typography', 'astro-blog'),
                'selector' => '{{WRAPPER}} .at-smart-tiles__group .at-post__title',
            ]
        );

        $this->add_responsive_control(
            'title_group_spacing',
            [
                'label' => __('Title Group Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-smart-tiles__group .at-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        /* Clasess */
        $classes[] = 'at-smart-tiles';

        /* get query argument */
        $the_query = new \WP_Query(Helper::query($settings));

        /* Header */
        echo HTML::header_block(array(
            'id' => 'header_' . $this->get_id(),
            'class' => 'rt-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
        ));

        /* Start LOOP */

        if ($the_query->have_posts()):

            $settings['class'] = 'at-post at-post--tiles';

            // start wrapper
            echo HTML::open(array(
                'id' => 'block_' . $this->get_id(),
                'class' => $classes,
                'data-page' => 1,
                'data-max-paged' => $the_query->max_num_pages,
            ));

            // start main
            echo HTML::open('at-smart-tiles__main');

            $index = 1;
            while ($the_query->have_posts()): $the_query->the_post();

                if ($index < 2) {
                    echo HTML::open('flex-item');
                    include dirname(__FILE__) . '/posts-tiles-view.php';
                    echo HTML::close();
                }

                $index++;
            endwhile;
            wp_reset_postdata();
            // end main
            echo HTML::close();

            // start group
            echo HTML::open('at-smart-tiles__group');
            $index = 1;
            while ($the_query->have_posts()): $the_query->the_post();

                if ($index > 1) {
                   include dirname(__FILE__) . '/posts-tiles-view.php';

                }

                $index++;
            endwhile;
            wp_reset_postdata();
            // end group
            echo HTML::close();
            // end wrapper
            echo HTML::close();

            wp_reset_postdata();

        else:
            _e('No Result', 'astro-blog');
        endif;
    }
}
