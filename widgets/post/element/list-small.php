<?php
$date_url   = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$author_url = get_author_posts_url(get_the_author_meta('ID'));
?>
<div class="flex-item">
	<div id="<?php echo 'post-'.get_the_ID()?>" <?php post_class('at-post at-post--list-small')?>>
		<?php if (has_post_thumbnail() && $settings['image_size_group'] != 'none'): ?>
			<div class="at-post__thumbnail rt-img rt-img--full">
				<?php the_post_thumbnail($settings['image_size_group'], 'img-responsive')?>
			</div>
		<?php endif;?>

		<div class="at-post__body">

			<h3 class="at-post__title"><a href="<?php the_permalink( )?>"><?php the_title()?></a></h3>
			
			<?php  include ASTRO_BLOG_TEMPLATE. 'widgets/post/element/meta.php' ?>
			
		</div>

	</div>
</div>