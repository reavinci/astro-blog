<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts extends \Astro_Blog\Elementor_Base
{
    protected $post_type = 'post';
    protected $post_taxonomy = 'category';

    public function get_name()
    {
        return 'astro-posts';
    }

    public function get_title()
    {
        return __('Posts Grid', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post';
    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_general(); //protected

        $this->style_body();
        $this->style_title();
        $this->style_badges();
        $this->style_meta();
        $this->style_excerpt();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.at-post__readmore',
            'label' => 'Read More',
        ));

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();
    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query($args = array())
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro-blog'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'astro-blog'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_type,
            ]
        );

        /**
         *  Number perpage not show if widget smart-tiles
         */
        if (empty($args['limit_perpage'])) {
            $this->add_control(
                'posts_per_page',
                [
                    'label' => __('Posts Number', 'astro-blog'),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 6,
                ]
            );
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', 'astro-blog'),
                    'type' => Controls_Manager::HEADING,
                ]
            );
        } else {
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', 'astro-blog'),
                    'type' => Controls_Manager::HEADING,
                    'separator' => 'before',
                ]
            );
        }

        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'astro-blog'),
                    'category' => __('Category', 'astro-blog'),
                    'manually' => __('Manually', 'astro-blog'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Categories', 'astro-blog'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'astro-blog'),
                    'wp_post_views_count' => __('Most Viewer', 'astro-blog'),
                    'comment_count' => __('Most Review', 'astro-blog'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'astro-blog'),
                    'DESC' => __('DESC', 'astro-blog'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'astro-blog'),
            ]
        );

        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro-blog'),
                    2 => __(2, 'astro-blog'),
                    3 => __(3, 'astro-blog'),
                    4 => __(4, 'astro-blog'),
                    6 => __(6, 'astro-blog'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro-blog'),
                'label_on' => __('On', 'astro-blog'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'astro-blog'),
            ]
        );

        $this->add_control(
            'readmore',
            [
                'label' => __('Read More', 'astro-blog'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Read More', 'astro-blog'),
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );

        $this->end_controls_section();
    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', 'astro-blog'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-blog'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-blog'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-blog'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .at-post__title,
                    {{WRAPPER}} .at-post__badges,
                    {{WRAPPER}} .at-post__meta,
                    {{WRAPPER}} .at-post__body' => 'text-align: {{VALUE}};',
                ],

            ]
        );


        $this->end_controls_section();
    }

    public function style_image()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Image Width', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 80,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );
        $this->end_controls_section();

    }

    public function style_body()
    {

        $this->start_controls_section(
            'style_body',
            [
                'label' => __('Body', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'body_background',
            [
                'label' => __('Background Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'body_padding',
            [
                'label' => __('Padding', 'astro-blog'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .at-post__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__title a' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__title a:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .at-post__title',
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label' => __('Title Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_badges()
    {
        $this->start_controls_section(
            'style_badges',
            [
                'label' => __('Badges', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('badges_tabs');
        $this->start_controls_tab(
            'badges_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );
        $this->add_control(
            'badges_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__badges a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'badges_background',
            [
                'label' => __('Background Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__badges a' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'badges_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'badges_color_hover',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__badges a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'badges_background_hover',
            [
                'label' => __('Background Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__badges a:hover' => 'background-color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

 

        $this->add_responsive_control(
            'badges_radius',
            [
                'label' => __('Badges Radius', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 3,
                    'unit' => 'px',
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .at-post__badges a' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_meta()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Meta', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('meta_tabs');
        $this->start_controls_tab(
            'meta_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );

        $this->add_control(
            'meta_link',
            [
                'label' => __('Link', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__meta a, {{WRAPPER}} .at-post__meta' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'meta_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'meta_link_hover',
            [
                'label' => __('Link', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__meta a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_control(
            'meta_color_divender',
            [
                'label' => __('Color Divender', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__meta-item::before' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'meta_margin',
            [
                'label' => __('Meta Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post__meta' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_excerpt()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .at-post__content',
            ]
        );

        $this->add_responsive_control(
            'excerpt_margin',
            [
                'label' => __('Excerpt Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/post/posts-view',
            'class' => 'at-post at-post--grid',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
    /* end class */
}
