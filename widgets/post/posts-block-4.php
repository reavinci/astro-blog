<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Astro_Blog\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_Block_4 extends Posts_Block
{
    public function get_name()
    {
        return 'astro-posts-block-4';
    }

    public function get_title()
    {
        return __('Posts Block 4', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-block-4';

    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_general();
        $this->style_image_block();
        $this->style_title_block();
        $this->style_meta_block();
        $this->style_badges();
        $this->style_excerpt();

    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_control(
            'pagination',
            [
                'label' => __('Pagination', 'astro-blog'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'none',
            ]
        );

        $this->add_responsive_control(
            'post_height',
            [
                'label' => __('Height', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 250,
                ],
                'range' => [
                    'px' => [
                        'min' => 100,
                        'max' => 700,
                        'step' => 1,
                    ],

                    '%' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],

                    'vh' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%', 'vh'],

                'selectors' => [
                    '{{WRAPPER}} .at-post--tiles .at-post__thumbnail' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'column',
            [
                'label' => __('Column', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => '2',
                'options' => [
                    '1' => __('1', 'astro-blog'),
                    '2' => __('2', 'astro-blog'),
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image on Main Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'image_size_group',
            [
                'label' => __('Image on Group Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

           $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );


        $this->end_controls_section();
    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Primary Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .flex-item,
                    {{WRAPPER}} .at-post-block__group .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                padding-right: calc({{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .at-post-block__main, .at-post-block__group' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .at-post-block__main .flex-item' => 'margin-bottom: {{SIZE}}{{UNIT}}',
                ],

            ]
        );

        $this->add_responsive_control(
            'general_group_margin',
            [
                'label' => __('Group Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post' => 'margin-bottom: {{SIZE}}{{UNIT}};',

                ],
            ]

        );


        $this->end_controls_tabs();

        $this->end_controls_section();
    }


   public function style_excerpt()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .at-post-block__main .at-post__content',
            ]
        );

        $this->add_responsive_control(
            'excerpt_margin',
            [
                'label' => __('Excerpt Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        /* Clasess */
        $classes[] = 'at-post-block at-post-block--tiles-vertical';
        $settings['class'] = 'at-post at-post--tiles';

        /* get query argument */
        $the_query = new \WP_Query(Helper::query($settings));

        /* Header */
        echo HTML::header_block(array(
            'id' => 'header_' . $this->get_id(),
            'class' => 'rt-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
        ));

        /* Start LOOP */

        if ($the_query->have_posts()):

            // open block
            echo HTML::open(array(
                'id' => 'block_' . $this->get_id(),
                'class' => $classes,
                'data-page' => 1,
                'data-max-paged' => $the_query->max_num_pages,
            ));

            // column
            $column_classes = ($settings['column'] == 2) ? 'flex flex-row flex-cols-md-6 flex-cols-sm-12' : 'flex flex-row flex-cols-sm-12';

            // open main
            echo HTML::open("at-post-block__main {$column_classes}");   

            $index = 1;
            while ($the_query->have_posts()): $the_query->the_post();

                if ($index <= $settings['column']) {
                    include dirname(__FILE__) . '/posts-tiles-view.php';

                }

                $index++;
            endwhile;
            wp_reset_postdata();
            // close main
            echo HTML::close();

            // open group
            echo HTML::open("at-post-block__group {$column_classes}");

            $index = 1;
            while ($the_query->have_posts()): $the_query->the_post();

                if ($index > $settings['column']) {
                    include dirname(__FILE__) . '/element/list-small.php';
                }

                $index++;
            endwhile;
            wp_reset_postdata();
            // close group
            echo HTML::close();
            // close block
            echo HTML::close();

            wp_reset_postdata();
        else:
            _e('No Result', 'astro-blog');
        endif;
    }
}
